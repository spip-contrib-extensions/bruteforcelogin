<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function genie_supprimer_bruteforce($time) {
	$date = date('Y-m-d H:i:s');
	$jobs = sql_allfetsel('c.id_bruteforce', 'spip_jobs AS a INNER JOIN spip_jobs_liens AS b INNER JOIN spip_bruteforces AS c ON (a.id_job=b.id_job AND b.objet=\'bruteforce\' AND b.id_objet=c.id_bruteforce)', 'a.date=' . sql_quote($date));
	foreach ($jobs as $job) {
		$supprimer_bruteforce = charger_fonction('supprimer_bruteforce', 'action');
		$supprimer_bruteforce($job['id_bruteforce']);
	}
}
