<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function bruteforcelogin_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['bruteforces'] = 'bruteforces';

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function bruteforcelogin_declarer_tables_objets_sql($tables) {

	$tables['spip_bruteforces'] = [
		'type' => 'bruteforce',
		'principale' => 'oui',
		'field' => [
			'id_bruteforce'		=> 'bigint(21) NOT NULL',
			'login'				=> 'text NOT NULL DEFAULT ""',
			'adresse_ip'		=> 'text NOT NULL DEFAULT ""',
			'date_bruteforce'	=> 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'maj'				=> 'TIMESTAMP'
		],
		'key' => [
			'PRIMARY KEY'        => 'id_bruteforce',
		],
		 #'titre' => '"" AS titre, "" AS lang',
		 #'date' => '',
		'champs_editables'  => ['login', 'adresse_ip', 'date_bruteforce'],
		'champs_versionnes' => ['login', 'adresse_ip', 'date_bruteforce'],
		'rechercher_champs' => [],
		'tables_jointures'  => [],
	];

	return $tables;
}
