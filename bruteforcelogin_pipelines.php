<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajout de contenu sur certaines pages,
 * notamment des formulaires de liaisons entre objets
 *
 * @pipeline affiche_milieu
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function bruteforcelogin_affiche_milieu($flux) {
	$texte = '';
	$e = trouver_objet_exec($flux['args']['exec']);

	// auteurs sur les bruteforces
	if (is_array($e) and !$e['edition'] and in_array($e['type'], ['bruteforce'])) {
		$texte .= recuperer_fond('prive/objets/editer/liens', [
			'table_source' => 'auteurs',
			'objet' => $e['type'],
			'id_objet' => $flux['args'][$e['id_table_objet']]
		]);
	}

	if ($texte) {
		if ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		} else {
			$flux['data'] .= $texte;
		}
	}

	return $flux;
}


/**
 * Ajout de liste sur la vue d'un auteur
 *
 * @pipeline affiche_auteurs_interventions
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function bruteforcelogin_affiche_auteurs_interventions($flux) {
	if ($id_auteur = intval($flux['args']['id_auteur'])) {
		$flux['data'] .= recuperer_fond('prive/objets/liste/bruteforces', [
			'id_auteur' => $id_auteur,
			'titre' => _T('bruteforce:info_bruteforces_auteur')
		], ['ajax' => true]);
	}
	return $flux;
}


/**
 * Passer par le pipeline formulaire vérifier pour compter les bruteforces
 *
 * @pipeline formulaire_verifier
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function bruteforcelogin_formulaire_verifier($flux) {
	if ($flux['args']['form'] == 'login') {
		$var_login = _request('var_login');
		$max = lire_config('bruteforcelogin/nbr_max', '5');
		$delais = lire_config('bruteforcelogin/delais', '60');

		if (!empty($flux['data'])) {
			$erreurs = [];
			// Si erreur, on enregistre le bruteforce
			$id_bruteforce = sql_insertq('spip_bruteforces', ['login' => $var_login, 'adresse_ip' => $_SERVER['REMOTE_ADDR'], 'date_bruteforce' => date('Y-m-d H:i:s')]);
			$id_job = job_queue_add('supprimer_bruteforce', _T('bruteforce:job_supprimer_bruteforce', ['id' => $id_bruteforce]), '', 'genie/', true, strtotime(date('Y-m-d H:i:s', strtotime("+ $delais minutes"))));
			job_queue_link($id_job, ['objet' => 'bruteforce', 'id_objet' => $id_bruteforce]);
			$nombre = sql_countsel('spip_bruteforces', 'login=' . sql_quote($var_login));
			// Nombre max de connection on bloque le formulaire
			if ($nombre < $max && defined('_BRUTEFORCELOGIN_LOG')) {
				$erreurs['message_erreur'] = singulier_ou_pluriel($nombre, _T('bruteforce:login_une_tentative', ['nbr' => $nombre, 'max' => $max]), _T('bruteforce:login_tentatives', ['nbr' => $nombre, 'max' => $max]));
			} else {
				$erreurs['message_erreur'] = '';
			}
			if ($nombre >= $max) {
				// on récupère le mail à partir du login
				$email = sql_getfetsel('email', 'spip_auteurs', 'login=' . sql_quote($var_login) . ' OR email=' . sql_quote($var_login));
				if (isset($email)) {
					include_spip('prive/formulaires/oubli');
					message_oubli($email, 'p');
				}
				//même si aucun email trouvé, toujours affiché que le mail a été renvoyé
				$erreurs['message_erreur'] = _T('bruteforce:reinitialiser_mdp_envoye');
				// quand on dépasse le nombre de tentatives+1 on annonce le blocage
				if ($nombre > $max) {
					$erreurs['message_erreur'] .= '<br>' . _T('bruteforce:compte_bloquer_tentative', ['delais' => $delais]);
				}
			}
			$flux['data'] = array_merge($flux['data'], $erreurs) ;
			return $flux;
		}
		if (empty($flux['data'])) {
			// Si pas d'erreur on vide les erreurs pour ce login
			$brutes = sql_allfetsel('id_bruteforce', 'spip_bruteforces', 'login=' . sql_quote($var_login));
			$brutes = array_column($brutes, 'id_bruteforce');
			sql_delete('spip_bruteforces', sql_in('id_bruteforce', $brutes));
			foreach ($brutes as $id_bruteforce) {
				$id_job = sql_getfetsel('id_job', 'spip_jobs', 'descriptif =' . sql_quote('Supprimmer le bruteforce #' . $id_bruteforce));
				job_queue_remove($id_job);
			}
		}
	}
	return $flux;
}
