<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// B
	'bruteforcelogin_titre' => 'Brute Force Login',

	// C
	'cfg_delais_minutes_blocage' => 'Indiquez le temps en minutes où le compte est bloqué',
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_nbr_max_tentative' => 'Combien de tentatives maximum avant de bloquer le compte ?',
	'cfg_titre_parametrages' => 'Paramétrages',

	// T
	'titre_page_configurer_bruteforcelogin' => 'Configuration du plugins Brute Force Login',
];
