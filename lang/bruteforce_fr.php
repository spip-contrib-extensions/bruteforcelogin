<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = [

	// A
	'ajouter_lien_bruteforce' => 'Ajouter ce brute-force',

	// C
	'champ_adresse_ip_label' => 'Adresse IP',
	'champ_date_bruteforce_label' => 'Date du brute-force',
	'compte_bloquer_tentative' => 'Votre compte est bloqué pendant @delais@ minutes. Veuillez contacter un administrateur.',
	'confirmer_supprimer_bruteforce' => 'Confirmez-vous la suppression de cet brute-force ?',

	// I
	'icone_creer_bruteforce' => 'Créer un brute-force',
	'icone_modifier_bruteforce' => 'Modifier ce brute-force',
	'info_1_bruteforce' => 'Un brute-force',
	'info_aucun_bruteforce' => 'Aucun brute-force',
	'info_bruteforces_auteur' => 'Les brute-forces de cet auteur',
	'info_nb_bruteforces' => '@nb@ brute-forces',

	// J
	'job_supprimer_bruteforce' => 'Supprimmer le bruteforce #@id@',

	// L
	'login_une_tentative' => 'Vous avez effectué @nbr@ tentative sur les @max@ autorisées.',
	'login_tentatives' => 'Vous avez effectué @nbr@ tentatives sur les @max@ autorisées.',

	// R
	'retirer_lien_bruteforce' => 'Retirer ce brute-force',
	'retirer_tous_liens_bruteforces' => 'Retirer tous les brute-forces',
	'reinitialiser_mdp_envoye' => 'Un lien de réinitialisation de votre mot de passe vous a été envoyé',

	// S
	'supprimer_bruteforce' => 'Supprimer cet brute-force',

	// T
	'texte_ajouter_bruteforce' => 'Ajouter un brute-force',
	'texte_changer_statut_bruteforce' => 'Ce brute-force est :',
	'texte_creer_associer_bruteforce' => 'Créer et associer un brute-force',
	'texte_definir_comme_traduction_bruteforce' => 'Ce brute-force est une traduction du brute-force numéro :',
	'titre_bruteforce' => 'Brute-force',
	'titre_bruteforces' => 'Brute-forces',
	'titre_bruteforces_rubrique' => 'Brute-forces de la rubrique',
	'titre_langue_bruteforce' => 'Langue de ce brute-force',
	'titre_logo_bruteforce' => 'Logo de ce brute-force',
	'titre_objets_lies_bruteforce' => 'Liés à ce brute-force',
];
