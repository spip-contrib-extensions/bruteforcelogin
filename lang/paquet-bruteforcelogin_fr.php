<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// B
	'bruteforcelogin_description' => 'Ce plugins sert à bloquer l\'accès au BO du site de cas de tentative infructueuse de connexion.',
	'bruteforcelogin_nom' => 'Brute Force Login',
	'bruteforcelogin_slogan' => 'Bloquer l\'accès au site si erreur de mot de passe',
];
